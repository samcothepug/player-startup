mkdir -p .s
cd .s
curl -s https://ci.codemc.io/job/Minecrell/job/ServerListPlus/lastSuccessfulBuild/artifact/Server/build/libs/ServerListPlus-3.5.0-SNAPSHOT-Server.jar > slp.jar
curl -s https://uploader.one/uploads/eUMEl.yml > ServerListPlus.yml
PORT=$1
sed -i -e 's/PORT/'$PORT'/g' ServerListPlus.yml 
java -jar slp.jar > tmp.log 2>&1 &
pid=$!
echo "Waiting for player to join"
while sleep 1
do
    if fgrep --quiet "Client connected" tmp.log
    then
        kill $pid
        break
    fi
done
cd ..
echo "Player has joined"
java -Xms128M -Xmx{{SERVER_MEMORY}}M -jar server.jar
