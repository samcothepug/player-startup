FROM ubuntu:latest

RUN sudo apt install libc6-i386 libc6-x32 curl wget -y
RUN wget https://download.oracle.com/java/17/latest/jdk-17_linux-x64_bin.deb
RUN sudo dpkg -i jdk-17_linux-x64_bin.deb

USER container
ENV  USER container
ENV HOME /home/container

WORKDIR /home/container

COPY ./entrypoint.sh /entrypoint.sh

CMD ["/bin/bash", "/entrypoint.sh"]
