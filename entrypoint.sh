#!/bin/bash
sleep 1

cd /home/container
wget https://gitlab.com/samcothepug/player-startup/-/raw/main/start.sh
java -version
MODIFIED_STARTUP=`eval echo $(echo ${STARTUP} | sed -e 's/{{/${/g' -e 's/}}/}/g')`
echo ":/home/container$ ${MODIFIED_STARTUP}"

# Run the Server
${MODIFIED_STARTUP}
